@extends('layouts.master')
@section('title')
Cast
@endsection

@section('subtitle')
Tambah Cast
@endsection

@section('content')

<a href="cast/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>

        @forelse ( $cast as $key => $items )
        <tr>
            <th scope="row">{{$key + 1 }}</th>
            <td>{{$items->nama}}</td>
            <td>{{$items->umur}}</td>
            <td>{{$items->bio}}</td>
            <td>
                <form action="/cast/{{$items->id}}" method="post">
                <a href="/cast/{{$items->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$items->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                
                
                    @csrf
                    @method('delete')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>    
        @empty
            <h1>Data Kosong</h1>
        @endforelse
     
     
    </tbody>
  </table>

@endsection