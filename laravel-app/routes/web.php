<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'Home']);
Route::get('/register', [AuthController::class,'register']);

Route::post('/bio', [AuthController::class,'send']);

Route::get('/table', function(){
    return view('page.table');
});
Route::get('/data-table', function(){
    return view('page.data-table');
});


// CRUD cast
// Create data

// rute untuk mengarah ke halaman form input cast
Route::get('/cast/create', [CastController::class,'create']);

// rute untuk mengarah ke database
Route::post('/cast',[CastController::class,'store']);

//Read data
// route untuk baca db

Route::get('/cast',[CastController::class,'index']);
Route::get('/cast/{id}',[CastController::class,'show']);


//Update data
Route::get('/cast/{id}/edit',[CastController::class,'edit']);
Route::put('/cast/{id}',[CastController::class,'update']);


//Delate data
Route::delete('/cast/{id}',[CastController::class,'destroy']);
