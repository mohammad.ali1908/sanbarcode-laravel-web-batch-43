<?php
Class Animal
{
    public $legs = 4;
    public $coldBlooded = "no";
    public $animalName;

    public function __construct($name)
    {
        $this->animalName = $name;
    }
}