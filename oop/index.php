<?php
require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$binatang = new Animal("shaun");

echo "Nama Binatang : " . $binatang->animalName . "<br>";
echo "Kaki Binatang : " . $binatang->legs . "<br>";
echo "Darah Dingin : " . $binatang->coldBlooded . "<br><br>";


$kodok = new Frog("buduk");

echo "Nama Binatang : " . $kodok->animalName . "<br>";
echo "Kaki Binatang : " . $kodok->legs . "<br>";
echo "Darah Dingin : " . $kodok->coldBlooded . "<br>";
echo $kodok->jump();

echo "<br>";
$sungokong  = new Ape("kera sakti");

echo "Nama Binatang : " . $sungokong ->animalName . "<br>";
echo "Kaki Binatang : " . $sungokong ->legs . "<br>";
echo "Darah Dingin : " . $sungokong ->coldBlooded . "<br>";
echo $sungokong ->yell();

?>